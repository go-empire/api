module.exports = {
   "type": "postgres",
   "host": "localhost",
   "port": 5432,
   "username": process.env.DB_USER,
   "password": process.env.DB_PASSWORD,
   "database": process.env.DB_NAME,
   "synchronize": true,
   "extra": {
      "charset": "utf8mb4_unicode_ci"
   },
   "logging": true,
   "logger": "file",
   "entities": [
      "dist/entity/*.js"
   ],
   "migrations": [
      "dist/migration/*.js"
   ],
   "subscribers": [
      "dist/subscriber/*.js"
   ],
   "cli": {
      "entitiesDir": "src/entity",
      "migrationsDir": "src/migration",
      "subscribersDir": "src/subscriber"
   }
}
