import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToOne,
    OneToMany,
    AfterLoad,
    getRepository,
    ManyToMany,
    OneToOne, JoinColumn
} from "typeorm";
import { Category } from './category';
import { User } from './user';
import config from '../config/common.config';
import {CommonService} from "../services/common.service";
import Container from "typedi";
import {FileService} from "../services/file.service";
import {Conversation} from "./conversation";

@Entity()
export class Message {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    text: string;

    // text | image; default = text
    @Column()
    type: string;

    @Column()
    senderId: string;

    @Column({ nullable: true })
    receiverId: string;

    @Column({ type: 'simple-array', nullable: true })
    admins: string[];

    @Column()
    conversationId: string;

    @CreateDateColumn({ type: "timestamptz" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    updatedAt: Date;

    @DeleteDateColumn({ type: "timestamptz" })
    deletedAt: Date;

    @ManyToOne((_) => User, user => user.messages) 
    sender: User;

    @ManyToOne((_) => User, user => user.messages)
    receiver: User;

    @ManyToOne((_) => Conversation, conversation => conversation.messages)
    conversation: Conversation;
}
