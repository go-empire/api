import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToOne,
    OneToMany,
    ManyToMany,
    JoinTable,
    AfterLoad, OneToOne, JoinColumn
} from "typeorm";
import { Adherent } from './adherent';
import { File } from "./file";
import { Notification } from "./notification";
import { Role } from './role';
import { Project } from './project';
import { Article } from './article';
import Container from "typedi";
import {CommonService} from "../services/common.service";
import {FileService} from "../services/file.service";
import {Conversation} from "./conversation";
import {Message} from "./message";

@Entity()
export class User {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: true })
    username: string;

    @Column({ nullable: true })
    fullname: string;

    @Column()
    email: string;

    @Column({ select: false })
    password: string;

    @Column({ nullable: true, default: '' })
    firstname: string;

    @Column({ nullable: true, default: '' })
    lastname: string;

    @Column({ nullable: true })
    birthday: Date;

    @Column({ nullable: true })
    profilePictureId: string;

    @Column({ nullable: true })
    job: string;

    @Column({ nullable: true })
    phone: string;

    @Column()
    isRepresentant: boolean;

    @Column({ default: false })
    loggedIn: boolean;

    @Column({ nullable: true })
    datejoined: Date;

    @Column({ nullable: true })
    adherentId: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;

    profilePictureUrl: string;


    @ManyToOne(_ => Adherent, adherent => adherent.users)
    adherent: Adherent;

    @OneToMany(_ => Notification, notification => notification.createdBy)
    notifications: Notification[];

    @OneToMany(_ => Project, project => project.submittedBy)
    projects: Project[];

    @OneToMany(_ => Article, article => article.createdBy)
    articles: Article[];

    @OneToMany(_ => File, file => file.createdBy)
    files: File[];

    @ManyToMany(_ => Role, role => role.users, { cascade: true })
    @JoinTable()
    roles: Role[];

    @OneToOne( (_) => Conversation, conversation => conversation.user, { cascade: true })
    conversation: Conversation;

    @OneToMany( (_) => Message, message => message.sender, { cascade: true })
    messages: Message[];

    @AfterLoad()
    async setProfileUrl() {
        const fileService = Container.get(FileService);

        if (this.profilePictureId) {
            this.profilePictureUrl = await fileService.fileUrl(this.profilePictureId);
        }
    }
}
