import { AfterLoad, Column, CreateDateColumn, DeleteDateColumn, Entity, getRepository, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { User } from './user';
import { CommonService } from '../services/common.service';
import { Container } from 'typedi';
import { File } from './file';
import {FileService} from "../services/file.service";

@Entity()
export class Article {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  title: string;

  @Column()
  content: string;

  @Column({ nullable: true })
  imageId: string;

  @UpdateDateColumn()
  updatedAt: Date;

  @CreateDateColumn()
  createdAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  imageUrl: string;

  @ManyToOne(_ => User, user => user.articles, { onDelete: 'SET NULL' })
  createdBy: User;

  @AfterLoad()
  async setImageUrl() {
    const fileService = Container.get(FileService);
    if (this.imageId) {
      this.imageUrl = `${await fileService.fileUrl(this.imageId)}`;
    }
  }
}
