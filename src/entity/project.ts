import {
  AfterLoad,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  getRepository,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { User } from "./user";
import { File } from "./file";
import Container, { Inject } from "typedi";
import { CommonService } from "../services/common.service";
import {FileService} from "../services/file.service";

@Entity()
export class Project {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column({ nullable: true })
  budget: number;

  @Column({ nullable: true })
  startDate: Date;

  @Column()
  duration: number;

  @Column({ default: 0 })
  status: number;

  @Column({ nullable: true })
  imageId: string;

  @Column()
  adherentId: string;

  @UpdateDateColumn({ type: "timestamptz" })
  updatedAt: Date;

  @CreateDateColumn({ type: "timestamptz" })
  createdAt: Date;

  @DeleteDateColumn({ type: 'timestamptz' })
  deletedAt: Date;

  imageUrl: string;

  // relations
  @ManyToOne((_) => User, (user) => user.projects, { onDelete: "SET NULL" })
  submittedBy: User;

  @AfterLoad()
  async setImageUrl() {
    const fileService = Container.get(FileService);
    this.imageUrl = this.imageId ? await fileService.fileUrl(this.imageId) : null;
  }
}
