import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, ManyToOne, OneToMany, AfterLoad, getRepository} from "typeorm";
import { Category } from './category';
import { User } from './user';
import config from '../config/common.config';
import {CommonService} from "../services/common.service";
import Container from "typedi";
import {FileService} from "../services/file.service";

@Entity()
export class Adherent {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @Column({ nullable: true })
    logoId: string;

    @Column()
    revenue: number;

    @Column({ nullable: true })
    phone: string;

    @Column({ nullable: true })
    email: string;

    @Column({ nullable: true })
    address: string;

    @Column()
    country: string;

    @Column()
    level: string;

    @Column({ nullable: true })
    activity: string;

    @Column({ default: 0 })
    points: number;

    @Column()
    categoryId: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;

    logo: string;


    @ManyToOne(_ => Category, category => category.adherents)
    category: Category;

    @OneToMany(_ => User, user => user.adherent)
    users: User[];


    @AfterLoad() async loadLogoUrl() {
        const fileService = Container.get(FileService);
        if (this.logoId) {
            this.logo = `${await fileService.fileUrl(this.logoId)}`;
        }
    }
}
