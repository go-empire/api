import { AfterLoad, Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { User } from './user';
import path from 'path';

@Entity()
export class File {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  filename: string;

  @Column({ default: "" })
  name: string;

  //kind : ["profile", "attachment"]
  @Column({ nullable: true })
  kind: string;

  @Column()
  mimetype: string;

  @Column()
  size: number;

  @Column({ nullable: true })
  owner: string;

  @Column({ nullable: true })
  createdById: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;


  @ManyToOne(_ => User, user => user.files)
  createdBy: User

  url: string;

  @AfterLoad()
  loadUrl() {
    // this.url = `https://api.empire-project.com/${this.filename}`;
    this.url = `http://localhost:3000/static/${this.filename}`;
  }
}
