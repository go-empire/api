import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToOne,
    OneToMany,
    AfterLoad,
    getRepository,
    ManyToMany,
    OneToOne, JoinColumn
} from "typeorm";
import { Category } from './category';
import { User } from './user';
import config from '../config/common.config';
import {CommonService} from "../services/common.service";
import Container from "typedi";
import {FileService} from "../services/file.service";
import {Message} from "./message";

@Entity()
export class Conversation {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    userId: string;

    @Column({ nullable: true, type: "simple-array"})
    admins: string[];

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;

    @OneToOne((_) => User, user => user.conversation)
    @JoinColumn()
    user: User;

    @OneToMany(() => Message, message => message.conversation)
    messages: Message[];
}
