import jwt from 'jsonwebtoken';
import { Context } from 'koa';
import { Service, Inject } from "typedi";
import { Repository } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { File } from '../entity/file';
import { Errors, RequestError } from '../common/errors';
import {CommonService} from "./common.service";

@Service()
export class FileService {

  @InjectRepository(File) private fileRepo: Repository<File>;
  @Inject() commonService: CommonService;

  onUpload = async (ctx: Context) => {
    const file = ctx.request['file'];
    const user = ctx.state.user;
    const { owner, kind ,name } = ctx.request.body;

    const data = {
      filename: file.filename,
      mimetype: file.mimetype,
      size: file.size,
      owner : owner != undefined ? owner : user.id,
      createdById: user.id,
      name: name ?? file.originalname,
      kind: kind ?? "attachment"
    }
    const saved = await this.fileRepo.save(data);
    ctx.body = await this.fileRepo.findOne(saved.id);
  }

  async fileUrl(id: string) {
    const file = await this.fileRepo.findOne(id);
    return file != undefined ? `${this.commonService.getMediaBaseUrl()}/${file.filename}`: null;
  }
}
