import { Service } from "typedi";
import config from '../config/common.config';

@Service()
export class CommonService {

  getMediaBaseUrl() {
    return `${process.env.BASE_URL}/${config.staticFilesPath}`;
  }
}
