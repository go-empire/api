import {Repository} from "typeorm";
import bcrypt from "bcrypt";
import {Inject, Service} from "typedi";
import {InjectRepository} from "typeorm-typedi-extensions";
import {User} from "../entity/user";
import {Errors, RequestError} from "../common/errors";
import {jwtService} from "./jwt.service";
import {Role} from "../entity/role";

@Service()
export class UserService {
  @InjectRepository(User) private userRepo: Repository<User>;
  @Inject() private jwtService: jwtService;

  async currentUser(id: string) {
    return await this.userRepo.findOne(id);
  }

  async assignRoles(id: string, roles: Role[]) {
    const user = await this.userRepo.findOne(id, { relations: ['roles'] });
    user.roles.push(...roles);
    return await this.userRepo.save(user);
  }

  async userRoles(id: string) {
    const user = await this.userRepo.findOne(id, { relations: ['roles'] });
    return user.roles;
  }

  async userPermissions(id: string) {
    const user = await this.userRepo.findOne(id, { relations: ['roles'] });
    return user.roles.flatMap(role => role.permissions.map(permission => permission.name));
  }

  async shouldResetPassword(email: string): Promise<boolean | null> {
    const user = await this.userRepo.findOne({ email });
    return user == undefined ? null : !user.loggedIn;
  }


  private async emailExist(email: string) {
    const user = await this.userRepo.findOne({ email });
    return user != undefined;
  }

  async getUsers(representantOnly: boolean) {
    return this.userRepo.find({ where: { isRepresentant: representantOnly }});
  }

  async updateUser(id: string, data: unknown) {
    return await this.userRepo.update(id, data);
  }

  async deleteUser(id: string) {
    return await this.userRepo.softDelete(id);
  }

  async createUser(data: User) {
    if (await this.emailExist(data.email)) {
      throw new RequestError("Email already exists", Errors.emailExistAlready);
    }

    const temporaryPassword = Math.random().toString(36).slice(-10);
    const passwordHashed = bcrypt.hashSync(temporaryPassword, 10);
    const user = this.userRepo.create(data);
    user.password = passwordHashed;
    const userSaved = await this.userRepo.save(user);

    return {
      user: userSaved,
      password: temporaryPassword,
    };
  }

  async authUser(email: string, password: string) {
    if (!(await this.emailExist(email))) {
      throw new RequestError("Wrong credentials", Errors.wrongCredentials);
    }

    const user = await this.userRepo.findOne({ select: ["id", "email", "password", "loggedIn"], where: { email } });

    const machted = await bcrypt.compare(password, user.password);
    if (!machted) {
      throw new RequestError("Wrong credentials", Errors.wrongCredentials);
    }

    if (!user.loggedIn) {
      throw new RequestError("User have to change the password", Errors.passwordNotChanged);
    }

    const token = this.jwtService.sign({ id: user.id, email: user.email });

    return {
      token,
      id: user.id
    }
  }

  async resetPassword(email: string, password: string, newPassword: string) {
    if (!(await this.emailExist(email))) {
      throw new RequestError("Wrong credentials", Errors.wrongCredentials);
    }

    const user = await this.userRepo.findOne({ select: ["id", "email", "password", "loggedIn"], where: { email } });
    const machted = await bcrypt.compare(password, user.password);
    if (!machted) {
      throw new RequestError("Wrong credentials", Errors.wrongCredentials);
    }

    const hash = await bcrypt.hash(newPassword, 10);
    user.password = hash;
    user.loggedIn = true;
    await this.userRepo.save(user);

    const token = this.jwtService.sign({ id: user.id, email: user.email });

    return {
      token,
      id: user.id
    }
  }
}
