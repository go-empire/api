import { Service } from 'typedi';
import nodemailer from 'nodemailer';
import mailConfig from '../config/mail.config';
import handlebars from 'handlebars';
import fs from 'fs';
import path from 'path';

@Service()
export class mailService {

  transporter = nodemailer.createTransport({
    host: mailConfig.host,
    port: 465,
    secure: true,
    auth: {
      user: mailConfig.auth.user,
      pass: mailConfig.auth.password
    }
  });

  async sendMail(template: string, data: any) {
    await this.transporter.sendMail({
      from: mailConfig.from,
      to: data.to,
      subject: this.mailSubject(template),
      html: this.getHtml(template, { password: data.password })
    });
  }

  private mailSubject(template: string) {
    switch(template) {
      case mailConfig.templates.userCreated:
        return `Création d'un nouveau compte`;
      default:
        return `Message de l'équipe Empire`;
    }
  }

  private getHtml(templateName: string, data: any): string {
    const file = fs.readFileSync(path.resolve(`src/templates/${templateName}.template.hbs`), 'utf-8');
    const template = handlebars.compile(file);
    return template(data);
  }
}