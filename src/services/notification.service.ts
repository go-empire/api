import jwt from 'jsonwebtoken';
import { Service, Inject } from "typedi";
import { Notification } from '../entity/notification';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Repository } from 'typeorm';
import { FireService } from './fire.service';

@Service()
export class NotificationService {

  @InjectRepository(Notification) private notificationRepo: Repository<Notification>;
  @Inject() fire: FireService;

  async getNotifications() {
    return await this.notificationRepo.find({
      order: { createdAt: 'DESC' }
    });
  }

  async add(data: unknown) {
    const notification: Notification = this.notificationRepo.create(data);
    const saved = await this.notificationRepo.save(notification);
    await this.sendNotification(saved);
    return saved;
  }

  private async sendNotification(data: Notification) {
    return await this.fire.sendNotification(data);
  }
}
