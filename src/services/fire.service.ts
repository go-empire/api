import admin from 'firebase-admin';
import {Service} from 'typedi';
import {Notification} from '../entity/notification';
import {messaging} from "firebase-admin/lib/messaging";
import MessagingOptions = messaging.MessagingOptions;

@Service()
export class FireService {

    constructor() {
        admin.initializeApp({
            credential: admin.credential.applicationDefault()
        });
    }

    async sendNotification(notification: Notification) {
        const message = {
            notification: {
                title: notification.title,
                body: notification.message.slice(40)
            },
            data: {
                title: notification.title,
                message: notification.message
            }
        }

        return await admin.messaging().sendToTopic(`admin-notifications`, message);
    }

    async sendPush(topic: string, message: any) {
        const notification = {
            notification: {
                title: message.title,
                body: message.message
            },
            // data: {
            //     title: message.title,
            //     message: message.message
            // },
        }

        const options: MessagingOptions = {
            priority: 'high',
            collapseKey: message.title,
            contentAvailable: true,
            android: {
                priority: "high",
            },
            // Add APNS (Apple) config
            apns: {
                payload: {
                    aps: {
                        contentAvailable: true,
                    },
                },
                headers: {
                    "apns-push-type": "background",
                    "apns-priority": "5", // Must be `5` when `contentAvailable` is set to true.
                    "apns-topic": "com.gosparna.empire", // bundle identifier
                },
            }
        }

        return await admin.messaging().sendToTopic(topic, notification, options);
    }
}
