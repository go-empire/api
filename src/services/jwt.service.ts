import jwt from 'jsonwebtoken';
import { Service, Inject } from "typedi";

@Service()
export class jwtService {
  
  sign(data: any) {
    return jwt.sign(data, process.env.JWT_SECRET, { expiresIn: '100y'});
  }
}