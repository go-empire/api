import {Inject, Service} from "typedi";
import config from '../config/common.config';
import {Server, Socket} from "socket.io";
import {Message} from "../entity/message";
import {InjectRepository} from "typeorm-typedi-extensions";
import {Repository} from "typeorm";
import {FireService} from "./fire.service";
import {User} from "../entity/user";
import { Conversation } from '../entity/conversation';

@Service()
export class SocketioService {

  private io: Server;
  @InjectRepository(Message) private messageRepository: Repository<Message>;
  @InjectRepository(User) private userRepository: Repository<User>;
  @InjectRepository(Conversation) private conversationRepository: Repository<Conversation>;
  @Inject() push: FireService;

  init(io: Server) {
    this.io = io;
    this.loadListeners();
  }

  loadListeners() {
    this.io.on('connection', (socket: Socket) => {
      socket.on('message', async (message: Partial<Message>) => {
        let conversation = await this.conversationRepository.findOne(message.conversationId);
        await this.messageRepository.save(message);
        this.io.emit('message', { ...message, admins: conversation.admins });
        const user = await this.userRepository.findOne(message.senderId);
        
        if (!user.isRepresentant && (conversation?.admins?.length ?? 0) <= 0) {
          conversation.admins = [user.id];
          conversation = await this.conversationRepository.save(conversation);
        }

        const payload = {
          title: user.isRepresentant && (conversation?.admins?.length ?? 0) <= 0 ? `${user.fullname} veut contacter un admin` : user.fullname,
          message: message.text,
        }
        await this.push.sendPush(`chat-${message.conversationId}`, payload);
      });
    })
  }
}
