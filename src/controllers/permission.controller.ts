import { Context } from "koa";
import { Service } from 'typedi';
import { InjectRepository } from "typeorm-typedi-extensions";
import { Role } from '../entity/role';
import { Repository } from "typeorm";
import { Permission } from '../entity/permission';

@Service()
export class PermissionController {

  @InjectRepository(Permission) permissionRepo: Repository<Permission>;

  get = async (ctx: Context) => {
    ctx.body = await this.permissionRepo.find();
  } 

  add = async (ctx: Context) => {
    const data = ctx.request.body;
    const permissions = this.permissionRepo.create(data);
    ctx.body = await this.permissionRepo.save(permissions);
  } 

  update = async (ctx: Context) => {
    const id = ctx.params.id;
    const data = ctx.request.body;
    ctx.body = await this.permissionRepo.update(id, data);
  } 

  delete = async (ctx: Context) => {
    const id = ctx.params.id;
    ctx.body = await this.permissionRepo.delete(id);
  } 
}