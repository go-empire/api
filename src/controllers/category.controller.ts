import { Context } from "koa";
import { getRepository, Repository } from "typeorm";
import { Category } from '../entity/category';
import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';

@Service()
export class CategoryController {

  @InjectRepository(Category) private categoryRepo: Repository<Category>

  all = async (ctx: Context) => {
    ctx.body = await this.categoryRepo.find();
  }

  add = async (ctx: Context) => {
    const data = ctx.request.body;
    ctx.body = await this.categoryRepo.save(this.categoryRepo.create(data));
  }

  update = async (ctx: Context) => {
    const data = ctx.request.body;
    await this.categoryRepo.update(ctx.params.id, data);
    ctx.body = this.categoryRepo.findOne(ctx.params.id);
  }
}