import { Context } from "koa";
import { Service } from 'typedi';
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { User } from '../entity/user';
import { Article } from '../entity/article';

@Service()
export class ArticleController {

  @InjectRepository(Article) articleRepository: Repository<Article>;
  @InjectRepository(User) userRepository: Repository<User>;

  get = async (ctx: Context) => {
    ctx.body = await this.articleRepository.find();
  }

  one = async (ctx: Context) => {
    const { id } = ctx.params.id;
    ctx.body = await this.articleRepository.findOne(id);
  }

  add = async (ctx: Context) => {
    const data = this.articleRepository.create(ctx.request.body);
    ctx.body = await this.articleRepository.save(data);
  } 

  update = async (ctx: Context) => {
    const id = ctx.params.id;
    const data = ctx.request.body;
    ctx.body = await this.articleRepository.update(id, data);
  } 

  delete = async (ctx: Context) => {
    const id = ctx.params.id;
    ctx.body = await this.articleRepository.softDelete(id);
  }
}