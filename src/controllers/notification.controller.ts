import {Context} from "koa";
import {Inject, Service} from 'typedi';
import {NotificationService} from '../services/notification.service';

@Service()
export class NotificationController {

  @Inject() private notificationService: NotificationService;

  get = async (ctx: Context) => {
    ctx.body = await this.notificationService.getNotifications();
  }

  add = async (ctx: Context) => {
    const { title, message } = ctx.request.body;
    ctx.body = await this.notificationService.add({title, message});
  } 
}
