import { Context } from "koa";
import { Service } from 'typedi';
import { InjectRepository } from "typeorm-typedi-extensions";
import { Role } from '../entity/role';
import { Repository } from "typeorm";
import { Permission } from '../entity/permission';
import { Project } from '../entity/project';
import { User } from '../entity/user';
import { privateEncrypt } from "crypto";

const status = {
  0: 'Projet crée',
  1: 'En cours de traitement',
  2: 'Accepté',
  3: 'Clôturé',
  4: 'Rejeté'
}

@Service()
export class ProjectController {

  @InjectRepository(Project) projectRepository: Repository<Project>;
  @InjectRepository(User) userRepository: Repository<User>;

  getAll = async (ctx: Context) => {
    const { id } = ctx.state.user;
    const user = await this.userRepository.findOne(id);

    if (user.isRepresentant) {
      ctx.body = await this.projectRepository.find({ where: { adherentId: user.adherentId } });
      return;
    }

    ctx.body = await this.projectRepository.find();
  }

  one = async (ctx: Context) => {
    const { id } = ctx.params.id;
    ctx.body = await this.projectRepository.findOne(id);
  }

  add = async (ctx: Context) => {
    const data: Project = ctx.request.body;
    const { id } = ctx.state.user;
    const user = await this.userRepository.findOne(id);
    data.submittedBy = id;
    data.adherentId = user.adherentId;
    const project = this.projectRepository.create(data);
    const { id: projectId } = await this.projectRepository.save(project);
    ctx.body = await this.projectRepository.findOne(projectId);
  }

  update = async (ctx: Context) => {
    const id = ctx.params.id;
    const data = ctx.request.body;
    await this.projectRepository.update(id, data);
    ctx.body = await this.projectRepository.findOne(id);
  }

  delete = async (ctx: Context) => {
    const id = ctx.params.id;
    ctx.body = await this.projectRepository.softDelete(id);
  }
}
