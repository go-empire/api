import { Context } from "koa";
import { User } from "../entity/user";
import { Service, Inject } from "typedi";
import { UserService } from "../services/user.service";
import Router from "@koa/router";
import { mailService } from "../services/mail.service";
import mailConfig from "../config/mail.config";
import {InjectRepository} from "typeorm-typedi-extensions";
import {Repository} from "typeorm";

@Service()
export class UserController {
  @Inject() userService: UserService;
  @Inject() mailService: mailService;
  @InjectRepository(User) private userRepository: Repository<User>;

  me = async (ctx: Context) => {
    const { id } = ctx.state.user;

    ctx.body = await this.userService.currentUser(id);
  };

  assignRoles = async (ctx: Context) => {
    const { id } = ctx.state.user;
    const roles = ctx.request.body;
    const user = await this.userService.assignRoles(id, roles);
    ctx.body = user;
  }

  roles = async (ctx: Context) => {
    const { id } = ctx.state.user;
    const user = await this.userService.userRoles(id);
    ctx.body = user;
  }

  permissions = async (ctx: Context) => {
    const { id } = ctx.state.user;
    const user = await this.userService.userPermissions(id);
    ctx.body = user;
  }

  getOne = async (ctx: Context) => {
    ctx.body = await this.userRepository.findOne(ctx.params.id);
  }

  getAll = async (ctx: Context) => {
    const users = await this.userRepository.find({
      where: { isRepresentant: false },
      relations: ['roles']
    });

    ctx.body = users.map(user => {
      return {
        id: user.id,
        fullname: user.fullname,
        firstname: user.firstname,
        lastname: user.lastname,
        profilePictureUrl: user.profilePictureUrl,
        roles: user.roles.length > 1 ? `${user.roles.length} roles` : user.roles.reduce((acc, cur) => cur.name + " " + acc, "").trim()
      }
    });
  };

  representants = async (ctx: Context) => {
    ctx.body = await this.userService.getUsers(true);
  };

  add = async (ctx: Context) => {
    const data = await this.userService.createUser(ctx.request.body as User);
    await this.mailService.sendMail(mailConfig.templates.userCreated, {
      to: data.user.email,
      password: data.password,
    });
    ctx.body = data.user;
  };

  update = async (ctx: Context) => {
    await this.userService.updateUser(ctx.params.id, ctx.request.body);
    ctx.body = await this.userService.currentUser(ctx.params.id);
  };

  delete = async (ctx: Context) => {
    ctx.body = await this.userService.deleteUser(ctx.params.id);
  };
}
