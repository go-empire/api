import {Context} from "koa";
import {Inject, Service} from "typedi";
import {UserService} from "../services/user.service";
import {InjectRepository} from "typeorm-typedi-extensions";
import {Not, Repository} from "typeorm";
import {Conversation} from "../entity/conversation";
import {User} from "../entity/user";
import {Message} from "../entity/message";

@Service()
export class ChatController {
    @Inject() private userService: UserService;
    @InjectRepository(Conversation) private conversationRepository: Repository<Conversation>;
    @InjectRepository(User) private userRepository: Repository<User>;
    @InjectRepository(Message) private messageRepository: Repository<Message>;

    getUserConversation = async (ctx: Context) => {
        const otherId = ctx.params.id;
        const userId = ctx.state.user.id;
        const currentUser = await this.userRepository.findOne(userId);

        let userConversation = await this.conversationRepository.findOne({
            where: {userId: otherId}
        })

        if (userConversation == undefined) {
            userConversation = await this.conversationRepository.save({
                userId: otherId
            });

            if (!currentUser.isRepresentant) {
                userConversation.admins = [currentUser.id];
            }
        }

        const user = await this.userRepository.findOne(otherId);
        ctx.body = {
            conversationId: userConversation.id,
            memberId: user.id,
            admins: userConversation.admins,
            username: user.username,
            firstname: user.firstname,
            lastname: user.lastname,
            fullname: user.fullname,
            profileUrl: user.profilePictureUrl
        }
    };

    conversations = async (ctx: Context) => {
        const conversations = await this.conversationRepository.find({
            where: { userId: Not(ctx.state.user.id) },
            relations: ['user']
        });
        ctx.body = conversations.map((conversation) => {
            return {
                conversationId: conversation.id,
                memberId: conversation.user.id,
                admins: conversation.admins,
                username: conversation.user.username,
                firstname: conversation.user.firstname,
                lastname: conversation.user.lastname,
                fullname: conversation.user.fullname,
                profileUrl: conversation.user.profilePictureUrl
            }
        });
    };

    messages = async (ctx: Context) => {
        ctx.body = await this.messageRepository.find({
            where: {conversationId: ctx.params.conversationId},
            order: {createdAt: 'DESC'}
        });
    }
}
