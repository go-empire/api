import { Service } from "typedi";
import { Context } from 'koa';
import { InjectRepository } from "typeorm-typedi-extensions";
import { Adherent } from '../entity/adherent';
import { Repository } from 'typeorm';

@Service()
class DashboardController {

  @InjectRepository(Adherent) private adherentRepository: Repository<Adherent>;

}