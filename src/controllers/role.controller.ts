import { Context } from "koa";
import { Service } from 'typedi';
import { InjectRepository } from "typeorm-typedi-extensions";
import { Role } from '../entity/role';
import { Repository } from "typeorm";
import { Permission } from '../entity/permission';

@Service()
export class RoleController {

  @InjectRepository(Role) roleRepo: Repository<Role>;

  get = async (ctx: Context) => {
    ctx.body = await this.roleRepo.find();
  } 

  add = async (ctx: Context) => {
    const data: Role = ctx.request.body;
    const role = this.roleRepo.create(data);
    ctx.body = await this.roleRepo.save(role);
  } 

  update = async (ctx: Context) => {
    const id = ctx.params.id;
    const data = ctx.request.body;
    ctx.body = await this.roleRepo.update(id, data);
  } 

  delete = async (ctx: Context) => {
    const id = ctx.params.id;
    ctx.body = await this.roleRepo.delete(id);
  } 
}