import { Context } from "koa";
import { Service, Inject } from "typedi";
import { UserService } from "../services/user.service";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { Adherent } from "../entity/adherent";

@Service()
export class AdherentController {
  @Inject() private userService: UserService;
  @InjectRepository(Adherent) private adherentRepo: Repository<Adherent>;

  all = async (ctx: Context) => {
    console.log(ctx.state);
    var adherents = await this.adherentRepo.find({ relations: ["users", 'category'] });
    ctx.body = adherents.map((adherent) => {
      const representants = adherent.users.length;
      delete adherent.users;

      return {
        representants,
        ...adherent,
        category: adherent.category.name,
      };
    });
  };

  representants = async (ctx: Context) => {
    const { id } = ctx.params;

    const adherents = await this.adherentRepo.findOne(id, { relations: ['users'] });
    ctx.body = adherents.users;
  }

  addPoints = async (ctx: Context) => {
    const { points, adherentId } = ctx.request.body;
    const adherent = await this.adherentRepo.findOne(adherentId);
    adherent.points += Number(points);
    await this.adherentRepo.update(adherentId, { points: adherent.points });
    ctx.body = await this.adherentRepo.findOne(adherentId);
  }

  add = async (ctx: Context) => {
    const data = ctx.request.body;
    const { id } = await this.adherentRepo.save(data);
    ctx.body = await this.adherentRepo.findOne(id);
  };

  update = async (ctx: Context) => {
    const data = ctx.request.body;
    await this.adherentRepo.update(ctx.params.id, data);
    ctx.body = await this.adherentRepo.findOne(ctx.params.id);
  };

  delete = async (ctx: Context) => {
    const result = await this.adherentRepo.softDelete(ctx.params.id);
    ctx.body = result;
  };
}
