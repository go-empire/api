import { BaseContext, Context } from "koa";
import { Service, Inject } from 'typedi';
import { Errors, RequestError } from "../common/errors";
import { UserService } from "../services/user.service";

@Service()
export class AuthController {

  @Inject() userService: UserService;

  signin = async (ctx: Context) => {
    const { email, password } = ctx.request.body;
    const { token, id } = await this.userService.authUser(email, password);
    ctx.body = { token, id };
  }

  checkResetEmail = async (ctx: Context) => {
    const { email } = ctx.params;
    const shouldResetPassword = await this.userService.shouldResetPassword(email);
    if (shouldResetPassword == null) throw new RequestError("Invalid email", Errors.emailNotFound);

    ctx.body = shouldResetPassword;
  }

  resetPassword = async (ctx: Context) => {
    const { email, password, newPassword } = ctx.request.body;

    if (email == null || password == null || newPassword == null) {
      throw new RequestError("Wrong credentials", Errors.wrongCredentials);
    }

    const { token, id } = await this.userService.resetPassword(email, password, newPassword);
    ctx.body = { token, id };
  }
}
