import Router from '@koa/router';
import { Container, Service, Inject } from 'typedi';
import { NotificationController } from '../controllers/notification.controller';

@Service()
export class NotificationRoutes {

  private router = new Router();

  @Inject() private notification: NotificationController;

  get routes() {
    this.router.get("/", this.notification.get);
    this.router.post("/", this.notification.add);
    return this.router.routes();
  }
}