import Router from '@koa/router';
import { Service, Inject } from 'typedi';
import { AdherentController } from '../controllers/adherent.controller';
import {ChatController} from "../controllers/chat.controller";

@Service()
export class ChatRoutes {

  private router = new Router();

  @Inject() private chat: ChatController;

  get routes() {
    this.router.get("/user/:id/conversation", this.chat.getUserConversation);
    this.router.get("/conversations", this.chat.conversations);
    this.router.get("/messages/:conversationId", this.chat.messages);
    return this.router.routes();
  }
}
