import Router from '@koa/router';
import { Container, Service, Inject } from 'typedi';
import { RoleController } from '../controllers/role.controller';

@Service()
export class RoleRoutes {

  private router = new Router();

  @Inject() private role: RoleController;

  get routes() {
    this.router.get("/", this.role.get);
    this.router.post("/", this.role.add);
    this.router.patch("/:id", this.role.update);
    this.router.delete("/:id", this.role.delete);
    return this.router.routes();
  }
}