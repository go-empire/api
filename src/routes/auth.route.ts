import Router from '@koa/router';
import { AuthController } from "../controllers/auth.controller";
import { Service, Inject } from 'typedi';

@Service()
export class AuthRoutes {

  private router = new Router();

  @Inject() private auth: AuthController;

  get routes() {
    this.router.post("/signin", this.auth.signin);
    this.router.post("/reset", this.auth.resetPassword);
    this.router.get("/check/:email", this.auth.checkResetEmail);
    return this.router.routes();
  }
}

