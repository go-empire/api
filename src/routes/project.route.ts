import Router from '@koa/router';
import { Service, Inject } from 'typedi';
import { ProjectController } from '../controllers/project.controller';

@Service()
export class ProjectRoutes {

  private router = new Router();

  @Inject() private project: ProjectController;

  get routes() {
    this.router.get("/", this.project.getAll);
    this.router.get("/:id", this.project.one);
    this.router.post("/", this.project.add);
    this.router.patch("/:id", this.project.update);
    this.router.delete("/:id", this.project.delete);
    return this.router.routes();
  }
}