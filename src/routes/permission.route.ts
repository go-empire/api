import Router from '@koa/router';
import { Service, Inject } from 'typedi';
import { PermissionController } from '../controllers/permission.controller';

@Service()
export class PermissionRoutes {

  private router = new Router();

  @Inject() private permission: PermissionController;

  get routes() {
    this.router.get("/", this.permission.get);
    this.router.post("/", this.permission.add);
    this.router.patch("/:id", this.permission.update);
    this.router.delete("/:id", this.permission.delete);
    return this.router.routes();
  }
}