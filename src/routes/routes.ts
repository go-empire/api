import Router from "@koa/router";
import multer from '@koa/multer';
import path from 'path';
import { Service, Inject } from 'typedi';
import { AdherentRoutes } from "./adherent.route";
import { AuthRoutes } from "./auth.route";
import { CategoryRoutes } from "./category.route";
import { NotificationRoutes } from "./notification.route";
import { UserRoutes } from "./user.route";
import { FileService } from "../services/file.service";
import { v4 as uuidv4 } from 'uuid';
import { RoleRoutes } from "./role.route";
import { PermissionRoutes } from "./permission.route";
import { ProjectRoutes } from "./project.route";
import { ArticleRoutes } from "./article.route";
import {ChatRoutes} from "./chat.route";

@Service()
export class Routes {
  private router = new Router();
  storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
      const ext = path.extname(file.originalname);
      cb(null, `${uuidv4()}${ext}`);
    }
  })
  private upload = multer({ storage: this.storage });

  @Inject() private fileService: FileService;
  @Inject() private user: UserRoutes;
  @Inject() private auth: AuthRoutes;
  @Inject() private notification: NotificationRoutes;
  @Inject() private category: CategoryRoutes;
  @Inject() private adherent: AdherentRoutes;
  @Inject() private role: RoleRoutes;
  @Inject() private permssions: PermissionRoutes;
  @Inject() private projects: ProjectRoutes;
  @Inject() private articles: ArticleRoutes;
  @Inject() private chat: ChatRoutes;


  get routes() {
    this.router.get("/", async (ctx) => (ctx.body = new Date()));
    this.router.get("/test", async (ctx) => {
      ctx.body = "";
    });
    this.router.post("/file", this.upload.single('file'), this.fileService.onUpload);
    this.router.use("/auth", this.auth.routes);
    this.router.use("/categories", this.category.routes);
    this.router.use("/users", this.user.routes);
    this.router.use("/notifications", this.notification.routes);
    this.router.use("/adherents", this.adherent.routes);
    this.router.use("/roles", this.role.routes);
    this.router.use("/permissions", this.permssions.routes);
    this.router.use("/projects", this.projects.routes);
    this.router.use("/articles", this.articles.routes);
    this.router.use("/chat", this.chat.routes);
    return this.router.routes();
  }
}
