import Router from '@koa/router';
import { Service, Inject } from 'typedi';
import { ArticleController } from '../controllers/article.controller';

@Service()
export class ArticleRoutes {

  private router = new Router();

  @Inject() private article: ArticleController;

  get routes() {
    this.router.get("/", this.article.get);
    this.router.get("/:id", this.article.one);
    this.router.post("/", this.article.add);
    this.router.patch("/:id", this.article.update);
    this.router.delete("/:id", this.article.delete);
    return this.router.routes();
  }
}