import Router from '@koa/router';
import { Container, Service, Inject } from 'typedi';
import { UserController } from '../controllers/user.controller';

@Service()
export class UserRoutes {

  private router = new Router();

  @Inject() private user: UserController;

  get routes() {
    this.router.get("/", this.user.getAll);
    this.router.get("/me", this.user.me);
    this.router.get("/permissions", this.user.permissions);
    this.router.get("/roles", this.user.roles);
    this.router.get("/representants", this.user.representants);
    this.router.get("/:id", this.user.getOne);
    this.router.post("/", this.user.add);
    this.router.post("/roles", this.user.assignRoles);
    this.router.patch("/:id", this.user.update);
    this.router.delete("/:id", this.user.delete);
    return this.router.routes();
  }
}
