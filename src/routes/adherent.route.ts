import Router from '@koa/router';
import { Service, Inject } from 'typedi';
import { AdherentController } from '../controllers/adherent.controller';

@Service()
export class AdherentRoutes {

  private router = new Router();

  @Inject() private adherent: AdherentController;

  get routes() {
    this.router.get("/", this.adherent.all);
    this.router.get("/:id/representants", this.adherent.representants);
    this.router.post("/", this.adherent.add);
    this.router.post("/add-points", this.adherent.addPoints);
    this.router.patch("/:id", this.adherent.update);
    this.router.delete("/:id", this.adherent.delete);
    return this.router.routes();
  }
}
