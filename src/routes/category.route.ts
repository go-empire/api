import Router from '@koa/router';
import { CategoryController } from "../controllers/category.controller";
import { Service, Inject } from 'typedi';

@Service()
export class CategoryRoutes {

  private router = new Router();

  @Inject() private category: CategoryController;

  get routes() {
    this.router.get("/", this.category.all);
    this.router.post("/", this.category.add);
    return this.router.routes();
  }
}