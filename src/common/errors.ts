export class RequestError extends Error {

  constructor(message: string, name: string = "Bad request", ) {
    super(message);
    this.name = name;
  }
}

export const Errors = {
  wrongCredentials: 'WrongCredentials',
  passwordNotChanged: 'PasswordNotChanged',
  emailNotFound: 'EmailNotFound',
  emailExistAlready: 'EmailExistAlready',
  invalidData: 'InvalidData'
}