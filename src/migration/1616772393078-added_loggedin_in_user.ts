import {MigrationInterface, QueryRunner} from "typeorm";

export class addedLoggedinInUser1616772393078 implements MigrationInterface {
    name = 'addedLoggedinInUser1616772393078'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "loggedIn" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "loggedIn"`);
    }

}
