import {MigrationInterface, QueryRunner} from "typeorm";

export class addedEmailColOnAdheerent1617784496907 implements MigrationInterface {
    name = 'addedEmailColOnAdheerent1617784496907'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "adherent" ADD "email" character varying`);
        await queryRunner.query(`ALTER TABLE "adherent" ADD "address" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "adherent" DROP COLUMN "address"`);
        await queryRunner.query(`ALTER TABLE "adherent" DROP COLUMN "email"`);
    }

}
