import {MigrationInterface, QueryRunner} from "typeorm";

export class deletedAtOnArticle1627608770835 implements MigrationInterface {
    name = 'deletedAtOnArticle1627608770835'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "article" ADD "deletedAt" TIMESTAMP`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "article" DROP COLUMN "deletedAt"`);
    }

}
