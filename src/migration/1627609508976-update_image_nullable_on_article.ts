import {MigrationInterface, QueryRunner} from "typeorm";

export class updateImageNullableOnArticle1627609508976 implements MigrationInterface {
    name = 'updateImageNullableOnArticle1627609508976'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "article" ALTER COLUMN "imageId" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "article"."imageId" IS NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "article"."imageId" IS NULL`);
        await queryRunner.query(`ALTER TABLE "article" ALTER COLUMN "imageId" SET NOT NULL`);
    }

}
