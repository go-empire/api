import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1616673172674 implements MigrationInterface {
    name = 'initial1616673172674'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "category" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "username" character varying, "email" character varying NOT NULL, "password" character varying NOT NULL, "firstName" character varying NOT NULL, "lastName" character varying NOT NULL, "birthday" TIMESTAMP, "profilePictureUrl" character varying, "job" character varying NOT NULL, "phone" character varying NOT NULL, "isRepresentant" boolean NOT NULL, "datejoined" TIMESTAMP, "adherentId" uuid NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "adherent" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "logo" character varying NOT NULL, "revenue" integer NOT NULL, "phone" character varying NOT NULL, "country" character varying NOT NULL, "level" character varying NOT NULL, "activity" character varying NOT NULL, "points" integer NOT NULL DEFAULT '0', "categoryId" uuid NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, CONSTRAINT "PK_a64f044ef11eddcd07a0788b239" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_c42e93024a2c21189b0be078459" FOREIGN KEY ("adherentId") REFERENCES "adherent"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "adherent" ADD CONSTRAINT "FK_a3147adbce0f5d941571e7d45d0" FOREIGN KEY ("categoryId") REFERENCES "category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "adherent" DROP CONSTRAINT "FK_a3147adbce0f5d941571e7d45d0"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_c42e93024a2c21189b0be078459"`);
        await queryRunner.query(`DROP TABLE "adherent"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "category"`);
    }

}
