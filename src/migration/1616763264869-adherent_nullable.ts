import {MigrationInterface, QueryRunner} from "typeorm";

export class adherentNullable1616763264869 implements MigrationInterface {
    name = 'adherentNullable1616763264869'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_c42e93024a2c21189b0be078459"`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "adherentId" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "user"."adherentId" IS NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_c42e93024a2c21189b0be078459" FOREIGN KEY ("adherentId") REFERENCES "adherent"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_c42e93024a2c21189b0be078459"`);
        await queryRunner.query(`COMMENT ON COLUMN "user"."adherentId" IS NULL`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "adherentId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_c42e93024a2c21189b0be078459" FOREIGN KEY ("adherentId") REFERENCES "adherent"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
