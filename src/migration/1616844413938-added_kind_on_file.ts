import {MigrationInterface, QueryRunner} from "typeorm";

export class addedKindOnFile1616844413938 implements MigrationInterface {
    name = 'addedKindOnFile1616844413938'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "file" ADD "kind" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "file" DROP COLUMN "kind"`);
    }

}
