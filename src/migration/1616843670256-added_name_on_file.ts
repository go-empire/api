import {MigrationInterface, QueryRunner} from "typeorm";

export class addedNameOnFile1616843670256 implements MigrationInterface {
    name = 'addedNameOnFile1616843670256'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "file" ADD "name" character varying NOT NULL DEFAULT ''`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "file" DROP COLUMN "name"`);
    }

}
