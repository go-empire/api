import {MigrationInterface, QueryRunner} from "typeorm";

export class logoNullableAdherent1616799370952 implements MigrationInterface {
    name = 'logoNullableAdherent1616799370952'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "adherent" ALTER COLUMN "logo" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "adherent"."logo" IS NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "adherent"."logo" IS NULL`);
        await queryRunner.query(`ALTER TABLE "adherent" ALTER COLUMN "logo" SET NOT NULL`);
    }

}
