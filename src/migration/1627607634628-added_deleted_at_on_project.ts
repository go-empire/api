import {MigrationInterface, QueryRunner} from "typeorm";

export class addedDeletedAtOnProject1627607634628 implements MigrationInterface {
    name = 'addedDeletedAtOnProject1627607634628'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project" DROP COLUMN "deletedAt"`);
    }

}
