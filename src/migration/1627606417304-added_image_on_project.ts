import {MigrationInterface, QueryRunner} from "typeorm";

export class addedImageOnProject1627606417304 implements MigrationInterface {
    name = 'addedImageOnProject1627606417304'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project" ADD "imageId" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project" DROP COLUMN "imageId"`);
    }

}
