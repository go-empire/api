import {MigrationInterface, QueryRunner} from "typeorm";

export class addedOwnerOnFile1616842068530 implements MigrationInterface {
    name = 'addedOwnerOnFile1616842068530'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "file" ADD "owner" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "file" DROP COLUMN "owner"`);
    }

}
