import {MigrationInterface, QueryRunner} from "typeorm";

export class articleOnArticle1627609403097 implements MigrationInterface {
    name = 'articleOnArticle1627609403097'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "article" ADD "imageId" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "article" DROP COLUMN "imageId"`);
    }

}
