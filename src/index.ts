import { createConnection, useContainer } from "typeorm";
import path from 'path';
import Koa from "koa";
import cors from "@koa/cors";
import jwt from 'koa-jwt';
import logger from "koa-logger";
import bodyParser from "koa-bodyparser";
import serve from 'koa-static';
import mount from 'koa-mount';
import { Routes } from "./routes/routes";
import { Container } from 'typeorm-typedi-extensions';
import { Server } from 'socket.io';
import {SocketioService} from "./services/socketio.service";
import {createServer} from "http";

useContainer(Container);

createConnection()
  .then(async (_) => {
    console.log("successfuly connected to database");

    const app = new Koa();
    const httpServer = createServer(app.callback());
    const io = new Server(httpServer);
    const socketioService = Container.get(SocketioService);
    socketioService.init(io);
    app.use(cors());
    app.use(async (ctx, next) => {
      try {
        await next();
      } catch(err: any | Error) {
        ctx.status = 400;
        ctx.body = {
          name: err.name,
          message: err.message
        }
      }
    });


    app.use(jwt({ secret: process.env.JWT_SECRET }).unless({ path: [/^\/auth|static/] }));

    app.use(mount('/static', serve(path.resolve() + '/uploads')));

    const router = Container.get(Routes);
    app.use(logger());

    app.use(bodyParser());

    app.use(router.routes);

    httpServer.listen(process.env.PORT);
  })
  .catch((error: string) => {
      console.error(error);
    console.error("La connexion à la base de donnée a échoué");
  });
